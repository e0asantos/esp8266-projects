# README #

##primero actualizar opkg
opkg update

##despues una segunda paqueteria
opkg install libopenssl

#libreria de threads
opkg install libpthread

#avahi para mdns responder
opkg install avahi-daemon

#mdns responder
opkg install mdnsresponder
opkg install libstdcpp



From ssh:
opkg update
opkg install avahi-daemon
Edit /etc/avahi/avahi-daemon.conf to have these new lines:
[server]
...
allow-interfaces=br-lan
enable-dbus=no

[reflector]
...
enable-reflector=yes
And finally, use the GUI System -> Startup to Restart and then Enable the avahi-daemon, or ssh and:
/etc/init.d/avahi-daemon start
/etc/init.d/avahi-daemon enable
Note: If this router is a secondary router (that is, within your firewall), you can turn on mDNS for your entire internal network (that is to run on both interfaces of the router), using the following in /etc/avahi/avahi-daemon.conf:
allow-interfaces=br-lan,eth0



Para instalar correctamente esta aplicacion, hay que asegurarse tener la "Linux image" correspondiente a la version del openwrt donde se va a instalar,
1)hay que activar en make menuconfig en la seccion de utilities=>PHK
2) Cuando se compile hay que copiar el binario al openwrt o copiar el instalable /bin/ar71xx/packages/base/PHK_1_ar71xx.ipk
3) Abrir binario y asegurarse de tener todas las librerias correctas, quizas haya que hacer un symlink de libc.so.0 a libc.so
4) opkg install avahi-daemon
5) Al iniciar verificar que el avahi-daemon tenga permiso de instalar, hay que modificar el archivo avahi-daemon.conf y agregar un flag en
enable-dbus=no
into [server] section of avahi-daemon.conf.
6) Configurar mdns responder y avahi para que inicien desde startup