local IR = 1
local lts, i, wave = 0, 0, {}

local keys = {}
keys['10100010000000100000100010101000'] = '1'
keys['10001010000000100010000010101000'] = '2'
keys['10101010000000100000000010101000'] = '3'
keys['10000010000000100010100010101000'] = '4'
keys['10000000000000100010101010101000'] = '5'
keys['10101000000000100000001010101000'] = '6'
keys['10101010000000000000000010101010'] = '7'
keys['10100010001000000000100010001010'] = '8'
keys['10100000100000000000101000101010'] = '9'
keys['10100000101000000000101000001010'] = '0'
keys['10001010001000000010000010001010'] = '*'
keys['10100010100000000000100000101010'] = '#'
keys['10000000101000000010101000001010'] = 'U'
keys['10000000100000000010101000101010'] = 'L'
keys['10001000101000100010001000001000'] = 'R'
keys['10001000001000100010001010001000'] = 'D'
keys['10000010101000000010100000001010'] = 'OK'

local function getKey()
    local data = ''
    local len = table.getn(wave)
    if len >= 60 then
        print("...." .. len)
        local pkey = 0
        local started = false
        for k, v in pairs(wave) do
            v = math.floor(v/100)
            print(v)
            if (v > 40 and v < 50) then
                started = true
            end
            pkey = v
            if started then
                if v > 300 then
                    started = false
                end
                --this is just to fix some random skipped edges
                if (v > 20 and v < 25) or v == 11 then
                    if v > 20 and v < 25 then
                        d = 17
                    else
                        d = 6
                    end
                    v1 = v - d
                    data = data .. '' .. math.floor(v1/10)

                    v2 = v - (v - d)
                    data = data .. '' .. math.floor(v2/10)
                else
                    if v < 40 then
                        data = data .. '' .. math.floor(v/10)
                    end
                end
            end
        end
        print(data)
        control = data:sub(0, 32)
        if control == '00000000000000000101010101010101' then
            data = data:sub(32, 63)
            print(len, data, keys[data] or '?')
        end
    end
    lts, i, wave = 0, 0, {}
end

local function onEdge(level, ts)
    -- print("edge")
    local time = ts - lts
    wave[i] = time
    i = i + 1
    if time > 75000 then
        tmr.create():alarm(350, tmr.ALARM_SINGLE, getKey)
    end
    lts = ts
end

gpio.mode(IR,gpio.INT)
gpio.trig(IR, "both", onEdge)