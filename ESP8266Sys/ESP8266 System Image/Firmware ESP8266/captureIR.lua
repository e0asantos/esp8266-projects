local irpin = 1 --gpio 14 servo
local lastTimestamp = 0
local waveform = {}
local i = 1

gpio.mode(irpin,gpio.INT)
gpio.trig(irpin, "both", function(level, ts)
    onEdge(level, ts)
end)

function onEdge(level, ts)
    waveform[i] = level
    -- waveform[i+1] = ts - lastTimestamp
    lastTimestamp = ts
    i = i+1   
end

-- Print out the waveform
function showWaveform ()
    if table.getn(waveform) > 65 then
        currentKey = ''
        for k,v in pairs(waveform) do
            -- print(k,v)
            currentKey = currentKey .. v
        end
        i = 1;
        print(currentKey)
        currentKey = nil
        waveform = {}
    end
end
tmr.create():alarm(1000, tmr.ALARM_AUTO, showWaveform)

print("Ready")